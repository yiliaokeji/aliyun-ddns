﻿using Furion.ConfigurableOptions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliyunDDNS.Models.Options
{
    public class AliyunOptions : IConfigurableOptions<AliyunOptions>
    {
        //[Required(ErrorMessage = "AccessKey不能为空")]
        public string? AccessKey { get; set; }
        //[Required(ErrorMessage = "AccessKeySecret不能为空")]
        public string? AccessKeySecret { get; set; }

        public void PostConfigure(AliyunOptions options, IConfiguration configuration)
        {

            var a = "";
        }
    }
}

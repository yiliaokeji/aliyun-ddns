﻿using AliyunDDNS.Models.Options;
using Furion;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliyunDDNS
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRemoteRequest();
            services.AddTaskScheduler();
            services.AddConfigurableOptions<AliyunOptions>();
            // 事件总线
            services.AddEventBus(options =>
            {
                // 不启用事件日志
                options.LogEnabled = false;
                // 事件执行器（失败重试）
                //options.AddExecutor<RetryEventHandlerExecutor>(); 
            });
        }
    }
}
